var express = require('express');
var router = express.Router();

/* GET login page. */
router.get('/', (req, res) => {
    if (req.session.passport.user != null) {
        req.logout();
        res.redirect('/');
    }
    else {
        res.redirect('/')
    }
});

module.exports = router;