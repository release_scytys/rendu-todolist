var express = require('express');
var router = express.Router();
var passport = require('passport');
var Account = require('../models/account');

/* GET register page. */
router.get('/', (req, res) => {
    res.render('register', {});
    res.render('register', { title: 'Création du compte' });
});

/* POST register page. */
router.post('/', (req, res, next) => {
    Account.register(new Account({ username: req.body.username }), req.body.password, (err, account) => {
        if (err) {
            return res.render('register', {
                title: "Signup - Error",
                error: err.message,
                lastEntries: req.body
            });
        }

        passport.authenticate('local')(req, res, () => {
            req.session.save((err) => {
                if (err) {
                    return next(err);
                }
                res.redirect('/dashboard');
            });
        });
    });
});

module.exports = router;