var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET chat page. */
router.get('/', function (req, res) {
    if (req.session.passport == undefined || req.session.passport.user == null) {
        res.redirect('/login');
    } else {
        res.render('settings', {
            user: req.user,
            title: 'Paramètres'
        });
    }
});

module.exports = router;