var express = require('express');
var router = express.Router();
var passport = require('passport');
var Task = require('../models/task');

/* GET dashboard page. */
router.get('/', function (req, res) {
    if (req.session.passport == undefined || req.session.passport.user == null) {
        res.redirect('/login');
    } else {
        Task.list(req.session.passport.user, function (err, docs) {
            res.render('dashboard', {
                user: req.user,
                tasks: docs
            });
        });
    }
});

/* PUT dashboard page. */
router.put('/', function (req, res) {
    console.log('PUT/');
    if (req.session.passport == undefined || req.session.passport.user == null)
        res.status(401).send();
    else {
        let task = req.body;
        task.username = req.session.passport.user;
        console.log(task);
        Task.add(task, (added) => {
            if (added == undefined)
                res.status(500).send();
            else {
                console.log('task added');
                console.log(added);
                res.render('taskListItem', added);
            }
        });
    }
});

/* DELETE dashboard page. */
router.delete('/', function (req, res) {
    if (req.session.passport == undefined || req.session.passport.user == null)
        res.status(401).send();
    else if (req.body.taskId == undefined)
        res.status(400).send();
    else {
        let taskId = req.body.taskId;

        Task.remove(taskId, (err, removed) => {
            if (removed.result.ok == 1)
                res.status(200).send(taskId);
            else
                res.status(500).send();
        });
    }
});

module.exports = router;