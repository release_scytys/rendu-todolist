var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET login page. */
router.get('/', (req, res) => {
    if (req.session.passport && req.session.passport.user != null) {
        res.redirect('/dashboard');
    } else {
        res.render('login', {
            user: req.user,
            title: 'Se connecter',
            subTitle: 'Come back please !'
        });
    }
});

/* POST login page. */
router.post('/', passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/register'
}));

module.exports = router;