#!/bin/sh

# DEBUT DU SCRIPT
echo "Demarrage du script"

echo "Update des dépots :"
apt-get update 1>/dev/null 2>/dev/null

echo "Installation de nodeJS :"
apt-get install -y nodejs 1>/dev/null 2>/dev/null

echo "Installation du Framework Express :"
npm install express 1>/dev/null 2>/dev/null

echo "Installation de Mongodb :"
apt-get install -y mongodb 1>/dev/null 2>/dev/null

echo "Installation de pm2 :"
npm -g install pm2 1>/dev/null 2>/dev/null

echo "installation des modules :"
npm install 1>/dev/null 2>/dev/null

# DEBUT DE MIGRATION BDD
#clear
#echo "ETAPE BDD"
#echo "Username de votre BDD MongoDB :"
#read username

#echo "Mot de passe de votre BDD MongoDB :"
#read passe

#echo "mongorestore en cours"
#mongorestore -u $username -p $passe 1>/dev/null 2>/dev/null

echo "ATTENTION : Pour la BDD nous n'avons pas pu faire de mongodump donc nous n'avons pas de bdd a fusioner avec mongorestore"


# FIN DU SCRIPT
