var mongoose = require('mongoose');

// Récupère le type Schema.
var Schema = mongoose.Schema;

// Instancie un nouvel objet Schema avec en paramètre un objet qui défini
// la structure d'une tâche. Ex: le nom d'utilisateur est de type String
// et est requis. La date à pour valeur par défaut la date actuelle.

/*var taskSchema = new Schema({
    username: { type: String, required: true },
    name: { type: String, required: true },
    date: { type: Date, default: Date.now, required: true }
});
*/

var taskSchema = new Schema({
    username: { type: String, required: true },
    order: { type: String, required: true },
    name: { type: String, required: true },
    Backcolor: { type: String, required: true },
    Fontcolor: { type: String, required: true },
    date: { type: String, required: true },
    time: { type: String, required: true }
});
// Création de notre type Task (modèle).
var Task = mongoose.model('Task', taskSchema);

function add(task, callback) {
    // Créer une nouvelle instance du modèle Task puis sauvegarde le document
    // dans la base de donnée.
    new Task(task).save((err, product) => {
        if (err)
        {
            console.log(err);
            callback();
        }
        else
            callback(product._doc)
    });
}

function remove(taskId, callback) {
    Task.remove({
        "_id": taskId
    }, callback);
}

function list(username, callback) {
    // Utilisation du constructeur de requête Mongoose :
    // Recherche les documents matchant avec l'objet passé en argument.
    // Tri les résultat par la date (plus récent au plus ancien).
    // Exécute la requête puis la fonction de callback.s
    Task.find({
        'username': username
    }).sort({ order: -1 }).exec(callback);
}

// Exports des fonctions devant être accessible à l'extérieur de ce fichier.
exports.list = list;
exports.remove = remove;
exports.add = add;