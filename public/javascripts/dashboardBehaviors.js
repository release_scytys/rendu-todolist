$(document).ready(function () {
    $(document).scroll(() => {
        var $header = $('header');

        if ($(this).scrollTop() > 20) {
            if (!$header.hasClass('shadow'))
                $header.addClass('shadow');
        }
        else if ($header.hasClass('shadow'))
            $header.removeClass('shadow');

    });

    $('#addButton').click(addTask);

    $(document).on('click', '.task p .glyphicon-trash',removeTask);
});

// Event Handlers
function onTaskAdded(task) {
    console.log(task);

    $(task).prependTo('#taskList').slideDown(1200);
}

function onTaskRemoved(taskId) {
    $('#' + taskId).slideUp(200, () => {
        $(this).remove();
    });
}

// API REST - Gestion des tâches
function addTask() {

    let task = {
        name: $('#taskName').val(),
        order: $('#taskOrder').val(),
        Backcolor: $('#taskBackColor').val(),
        Fontcolor: $('#taskFontColor').val(),
        date: $('#taskDate').val(),
        time: $('#taskTime').val()
    };
    // Ajout d'une tâche en appellant cette page en REST avec PUT.
    // data:    Récupère l'id de la tâche qui est dans l'attribut 'id' de la div parent.
    // success: Supprimer la tâche du DOM.
    $.ajax({
        url: '/dashboard',
        method: 'PUT',
        data: task,
        success: onTaskAdded,
        error: function (err) {
            console.log(err);
        }
    });
}

function removeTask() {
    // Suppression d'une tâche en appellant cette page en REST avec DELETE.
    // data:    Récupère l'id de la tâche qui est dans l'attribut 'id' de la div parent.
    // success: Supprimer la tâche du DOM.
    $.ajax({
        url: '/dashboard',
        method: 'DELETE',
        data: { taskId: $(this).closest(".task").attr('id') },
        success: (id) => {
            onTaskRemoved(id)
        },
        error: function (err) {
            console.log(err);
        }
    });
}