// Connexion à socket.io
            var socket = io.connect('http://10.31.1.4:3000');

            // On demande le pseudo, on l'envoie au serveur et on l'affiche dans le titre
            var pseudo = prompt('Quel est votre pseudo ?');
            socket.emit('new_client', pseudo);
            socket.emit('deco', pseudo);

            // Quand on reçoit un message, on l'insère dans la page
            socket.on('message', function(data) {
                insertMessage(data.pseudo, data.message)
            })

            // Quand un nouveau client se connecte, on affiche l'information
            socket.on('new_client', function(pseudo) {
                $('.chat').prepend("<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 message-group-connection'><span class='user-connection'>" + pseudo + " </span><span class='message-connection'>s'est connecté sur le chat !</span></div>");})

            socket.on('deco', function(pseudo) {
                $('.chat').prepend("<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 message-group-deconnection'><span class='user-deconnection'>" + pseudo + " </span><span class='message-connection'>s'est déconnecté sur le chat !</span></div>");})

            // Lorsqu'on envoie le formulaire, on transmet le message
            $('form').submit(function () {
                var message = $('#message').val();
                console.log('Message : ' + message);
                socket.emit('message', message); // Transmet le message aux autres
                insertMessage(pseudo, message); // Affiche le message aussi sur notre page
                $('#message').val('').focus(); // Vide la zone de Chat et remet le focus dessus
                return false; // Permet de bloquer l'envoi "classique" du formulaire
            });

            // Ajoute un message dans la page
            function insertMessage(pseudo, message) {
                $('.chat').prepend('<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 message-group"><span class="user">'+ pseudo + ' :</span><span class="message">'+ message +'</span></div>');
            }